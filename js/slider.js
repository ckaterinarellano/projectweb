//Almacenar slider en una variable
var slider=$('#slider');
//Almacenar botones 
var siguiente=$('#btn-next');
var anterior=$('#btn-prev');

//mover ultima imagen al primer lugar
$('#slider section:last').insertBefore('#slider section:first');
//Mostrar la primera imagen con un margen de -100%
slider.css('margin-left', '-'+100+'%');

function moverDerecha(){ 
    slider.animate({
        marginLeft:'-' + 200 + '%'
    } ,700, function(){
            $('#slider section:first').insertAfter('#slider section:last');
            slider.css('margin-left','-'+100+'%')
        });
}

function moverIzquierda(){ 
    slider.animate({
        marginLeft: 0
    } ,700, function(){
            $('#slider section:last').insertBefore('#slider section:first');
            slider.css('margin-left','-'+100+'%')
        });
}

//Cuando se da click a boton next llama a la funcion moverDerecha
siguiente.on('click', function(){
    moverDerecha();
});

anterior.on('click', function(){
    moverIzquierda();
});

function autoplay() {
    interval = setInterval(function(){
        moverDerecha();
    }, 5000);
}

autoplay();